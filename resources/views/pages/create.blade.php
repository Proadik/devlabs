@extends('layout')


@section('content')

    <div class="wrapper">
        <br><br>
        <h3 style="margin-bottom: 0;">Создание акции: </h3>

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            <img src="images/{{ Session::get('image') }}">
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ url('create') }}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="text" name="title" placeholder="Title">
            </div>
            <div class="form-group">
                <input type="text" name="price" placeholder="Price">
            </div>
            <div class="form-group">
                <input type="text" name="description" placeholder="Description">
            </div>
            <div class="form-group">
                <input type="text" name="square" placeholder="Sqrt ft.">
            </div>
            <div class="form-group">
                <input type="file" name="photo">
            </div>
            <div class="form-group">
                <button type="submit">Add</button>
            </div>

        </form>
    </div>

@endsection