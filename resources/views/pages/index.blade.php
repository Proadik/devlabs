@extends('layout')

@section('page')
    <section class="caption">
        <h2 class="caption">Find You Dream Home</h2>
        <h3 class="properties">Appartements - Houses - Almaty</h3>
    </section>
@endsection

@section('content')
    <section class="search">
        <div class="wrapper">
            <form action="#" method="post">
                <input type="text" id="search" name="search" placeholder="What are you looking for?"  autocomplete="off"/>
                <input type="submit" id="submit_search" name="submit_search"/>
            </form>
            <a href="#" class="advanced_search_icon" id="advanced_search_btn"></a>
        </div>

        <div class="advanced_search">
            <div class="wrapper">
                <span class="arrow"></span>
                <form action="#" method="post">
                    <div class="search_fields">
                        <input type="text" class="float" id="check_in_date" name="check_in_date" placeholder="Check In Date"  autocomplete="off">

                        <hr class="field_sep float"/>

                        <input type="text" class="float" id="check_out_date" name="check_out_date" placeholder="Check Out Date"  autocomplete="off">
                    </div>
                    <div class="search_fields">
                        <input type="text" class="float" id="min_price" name="min_price" placeholder="Min. Price"  autocomplete="off">

                        <hr class="field_sep float"/>

                        <input type="text" class="float" id="max_price" name="max_price" placeholder="Max. price"  autocomplete="off">
                    </div>
                    <input type="text" id="keywords" name="keywords" placeholder="Keywords"  autocomplete="off">
                    <input type="submit" id="submit_search" name="submit_search"/>
                </form>
            </div>
        </div><!--  end advanced search section  -->
    </section><!--  end search section  -->


    <section class="listings">
        <div class="wrapper">
            <ul class="properties_list">
                @foreach($data as $d)
                    <li>
                        <a href="{{ url('houses/'.$d->id) }}">
                            <img src="{{ url('img/'.$d->photo) }}" alt="" title="" class="property_img"/>
                        </a>
                        <span class="price">${{ $d->price }}</span>
                        <div class="property_details">
                            <h1>
                                <a href="#">{{ $d->title }}</a>
                            </h1>
                            <h2>{{ $d->description }} <span class="property_size">({{ $d->square }})</span></h2>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>	<!--  end listing section  -->


@endsection