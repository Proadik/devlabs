@extends('layout')

@section('page')
    <section class="caption">
        <h2 class="caption">{{ $data->title }}</h2>
        {{--<h3 class="properties">${{ $data->price }}</h3>--}}
    </section>
@endsection

@section('content')

<div class="wrapper" style="padding-bottom: 50px;">
    <figure style="float: left; width:50%; margin: 25px 0; background: url('{{url("img/".$data->photo)}}') no-repeat; background-size: cover; display: block; height: 500px;"></figure>
    <div style="float:right; width:45%; margin: 25px 0;">
        <h4>Цена: ${{ $data->price }}</h4>
        <h4>Описание: {{ $data->description }}</h4>
        <h4>Квадратура: {{ $data->square }} кв. м.</h4>
    </div>

    <br style="clear: both;">
</div>

@endsection