<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class PagesController extends Controller
{
    public function index(){

        $client = new Client([
            'base_uri' => 'http://devlabsapi.dev:81/public/api/',
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'houses');

        $data = \GuzzleHttp\json_decode($response->getBody());
        return view('pages.index', compact('data'));
    }

    public function get($id){

        $client = new Client([
            'base_uri' => 'http://devlabsapi.dev:81/public/api/',
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'houses/'.$id);
        $data = \GuzzleHttp\json_decode($response->getBody());

        return view('pages.page', compact('data'));
    }

    public function create(){
        return view('pages.create');
    }

    public function create_post(Request $request){
        $client = new Client([
            'base_uri' => 'http://devlabsapi.dev:81/public/api/',
            'timeout'  => 10.0,
        ]);

        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->file('photo')->getClientOriginalExtension();
        $request->file('photo')->move(public_path('img'), $imageName);
        $client->request('POST', 'create', [
            'query' => ['title' => $request->input('title'),
                        'price' => $request->input('price'),
                        'description' => $request->input('description'),
                        'square' => $request->input('square'),
                        'filename' => $imageName]
        ]);

        return back()
            ->with('success','You have successfully upload image.');
    }

}
